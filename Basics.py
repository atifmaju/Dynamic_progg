memo = []
for i in range(260):
    memo.append(-1)

def fib_dynamic(n):


    if n ==0:
        return 0
    if n ==1:
        return 1
    if memo[n] != -1:
        return memo[n]
    memo[n]=fib_dynamic(n-2)+fib_dynamic(n-1)
    return memo[n]




def fib(n):
    if n<=2:
        return 1

    return fib(n-1)+fib(n-2)

print(fib(250))

print(fib_dynamic(250))







